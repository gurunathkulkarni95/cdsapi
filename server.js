const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to CDS node js server" });
});

require("./app/routes/staff.routes.js")(app);
require("./app/routes/Designation.route")(app);
require("./app/routes/Department.route")(app);
require("./app/routes/organization.routes")(app);
require("./app/routes/organizationaddress.routes")(app);
require("./app/routes/enquiry.routes")(app);
require("./app/routes/contact.routes")(app);
const port = 4000;

// set port, listen for requests
app.listen(port, () => {
  console.log("Server is running on port :" + port);
});
