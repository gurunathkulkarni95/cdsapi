const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");


const Contact = function(contact){
    this.name = contact.name;
    this.phoneno = contact.phoneno;
    this.mobileno = contact.mobileno;
    this.whatsappno =contact.whatsappno;
    this.email =contact.email;
    this.remarks =contact.remarks;
    this.org_add_id = contact.org_add_id;
    this.organization_id=contact.organization_id;
    this.role_id = contact.role_id;
}

Contact.create = (newContact, result) => {
    sql.query(queryGenertor.CREATE_CONTACT_QUERY, newContact, (err, res) => {
      if (err) {
        console.log(" Contact error: ", err);
        result(err, null);
        return;
      }
  
      console.log("Contact Created: ",newContact);
      result(null,newContact );
    });
  };
  // get All Data
  Contact.getAll = (result) => {
    sql.query(queryGenertor.SELECT_CONTACT_QUERY, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      // console.log("staff: ", res);
      result(null, res);
    });
  };

  // Get By ID
  Contact.findById = (id, result) => {
    sql.query(`${queryGenertor.SELECT_CONTACT_BY_ID_QUERY} "${id}"`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        result(null, res[0]);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  };

  
Contact.getContactRoleById = async (id, result, callBack) => {
  sql.query(`${queryGenertor.GET_CONTACT_ROLE_BY_ID_QUERY} "${id}"`, (err, res) => {
  
    if (err) {
      console.log("error: ", err);
      return {
        status : false
      };
    } else {
      // console.log("data: ", res[0].roles)
      result.role = res[0].roles
      console.log("result: ", result)
      callBack();
    }
  });
};



// Update
Contact.updateById = (id, contact, result) => {
  console.log("CONTACT",contact)
  const {
    name,
    phoneno,
    mobileno,
    whatsappno,
    email,
    remarks,
    org_add_id,
    organization_id,
    role_id,
  } = contact;
  sql.query(
    queryGenertor.UPDATE_CONTACT_QUERY,
    [
      name,
      phoneno,
      mobileno,
      whatsappno,
      email,
      remarks,
      org_add_id,
      organization_id,
      role_id,
      id
    ],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated customer: ", res);
      result(null, { id: id, ...contact });
    }
  );
};


// delete API for Contact by ID
Contact.remove = (id, result) => {
  sql.query(queryGenertor.DELETE_CONTACT_QUERY, id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted contact with id: ", id);
    result(null, res);
  });
};

module.exports = Contact;