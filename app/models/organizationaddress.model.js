const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

// constructor
const OrganizationAddress = function (organization) {
  this.add_name = organization.add_name;
  this.address = organization.address;
  this.locality = organization.locality;
  this.city = organization.city;
  this.district = organization.district;
  this.postal = organization.postal;
  this.state = organization.state;
  this.country = organization.country;
  this.org_id = organization.org_id;
  this.latitude = organization.latitude;
  this.longitude = organization.longitude;
  this.time_created = organization.time_created;
  this.time_modified = organization.time_modified;
};

OrganizationAddress.create = (newOrganization, result) => {
  sql.query(queryGenertor.CREATE_ORGANIZATIONADDRESS_QUERY, newOrganization, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("OrganizationAddress Created: ", { id: res.insertId, ...newOrganization });
    result(null, { id: res.insertId, ...newOrganization });
  });
};

// get All Data
OrganizationAddress.getAll = (result) => {
  sql.query(queryGenertor.SELECT_ORGANIZATIONADDRESS_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    // console.log("Organization: ", res);
    result(null, res);
  });
};


module.exports = OrganizationAddress;
