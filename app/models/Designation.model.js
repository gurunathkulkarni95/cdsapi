const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

// constructor
const Designation = function (staff) {};

// get All Data
Designation.getDesignation = (result) => {
  sql.query(queryGenertor.DESIGNATION_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

module.exports = Designation;
