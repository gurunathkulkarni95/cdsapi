const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

const Enquiry = function (enquiry) {
  this.enquirytype = enquiry.enquirytype;
  this.enquirystatus = enquiry.enquirystatus;
  this.activitytype = enquiry.activitytype;
  this.sitesurveyschedule_date = enquiry.sitesurveyschedule_date;
  this.organization = enquiry.organization;
  this.contact = enquiry.contact;
  this.enquirysource = enquiry.enquirysource;
  this.scheduledate = enquiry.scheduledate;
  this.comment = enquiry.comment;
  this.remarks = enquiry.remarks;
  this.staff = enquiry.staff;
  this.org_address_id = enquiry.org_address_id;
};

Enquiry.getEnquiryStatusModel = (result) => {
  sql.query(
    queryGenertor.Select_Query_URL_Creater({
      tableName: "enquiryapp_enquirystatus",
    }),
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

// get All Data
Enquiry.getEnqTypes = (result) => {
  sql.query(queryGenertor.ENQUIRY_TYPES_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Enquiry.getEnqSources = (result) => {
  sql.query(queryGenertor.ENQUIRY_SOURCES_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Enquiry.getActivityTypes = (result) => {
  sql.query(
    queryGenertor.Select_Query_URL_Creater({
      tableName: "enquiryapp_activitytypes",
    }),
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      result(null, res);
    }
  );
};

Enquiry.activityCreate = (newActivity, result) => {
  console.log(
    "newActivity",
    `${queryGenertor.SELECT_ACTIVITY_TYPE_QUERY}${newActivity.type}`
  );

  sql.query(
    `${queryGenertor.SELECT_ACTIVITY_TYPE_QUERY}"${newActivity.type}"`,
    (err, res) => {
      if (err) {
        console.log("ERR", err);
        sql.query(
          queryGenertor.insert_Query_URL_Creater({
            tableName: "enquiryapp_activitytypes",
          }),
          newActivity,
          (err, res) => {
            if (err) {
              console.log(" Contact error: ", err);
              result(err, null);
              return;
            }
            console.log("Activity Created: ", newActivity);
            result(null, newActivity);
          }
        );
      } else {
        result(true, null);
      }
    }
  );
};

Enquiry.activityDelete = (id, result) => {
  sql.query(
    queryGenertor.delete_QUERY_URL_Creater({
      tableName: "enquiryapp_activitytypes",
      whereClause: "id",
    }),
    id,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }
      console.log("deleted Activity with id: ", id);
      result(null, res);
    }
  );
};

Enquiry.create = (newEnquiry, result) => {
  sql.query(
    queryGenertor.insert_Query_URL_Creater({ tableName: "enquiry" }),
    newEnquiry,
    (err, res) => {
      if (err) {
        console.log(" Enquiry error: ", err);
        result(err, null);
        return;
      }

      console.log("Enquiry Created: ", newEnquiry);
      result(null, newEnquiry);
    }
  );
};

// get All Data
Enquiry.getEnquiries = (Organization, result) => { 
  if (Organization.organization) {
    sql.query(
      `${queryGenertor.SELECT_SEARCH_ENQUIRY_QUERY}"${Organization.organization}"`,
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        console.log("result", res);
        result(null, res);
      }
    );
  } else {
    sql.query(
      queryGenertor.Select_Query_URL_Creater({ tableName: "enquiry" }),
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        console.log("resultAll", res);
        result(null, res);
      }
    );
  }
};

Enquiry.findById = (id, result) => {
  sql.query(`${queryGenertor.SELECT_ENTQUIRY_BY_ID_QUERY} "${id}"`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


Enquiry.getReqLists = (result) => {
  sql.query(
  queryGenertor.Select_Query_URL_Creater({ tableName: "enquiryapp_request" }),(err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Enquiry.updateById =(id,enquiry,result)=>{
  const {
    enquirytype ,
    enquirystatus ,
    activitytype ,
    sitesurveyschedule_date ,
    organization ,
    contact ,
    enquirysource ,
    scheduledate ,
    comment ,
    remarks,
    staff,
    org_address_id,
    time_created,
    time_modified
  } = enquiry;
  sql.query(
    queryGenertor.UPDATE_ENQUIRY_QUERY,
    [
      enquirytype ,
      enquirystatus ,
      activitytype ,
      sitesurveyschedule_date ,
      organization ,
      contact ,
      enquirysource ,
      scheduledate ,
      comment ,
      remarks,
      staff,
      org_address_id,
      id
    ],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }
      console.log("updated enquiry: ", res);
      result(null, { id: id, ...enquiry });
    }
  );
}
module.exports = Enquiry;
