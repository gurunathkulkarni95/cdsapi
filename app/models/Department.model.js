const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

// constructor
const Department = function (staff) {};

// get All Data
Department.getDepartment = (result) => {
  sql.query(queryGenertor.DEPARTMENT_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

// get All Data
Department.getEmpType = (result) => {
    sql.query(queryGenertor.EMP_TYPE_QUERY, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      result(null, res);
    });
  };

module.exports = Department;
