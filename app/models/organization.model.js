const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

// constructor
const Organization = function (organization) {
  this.org_type = organization.org_type;
  this.vendor_code = organization.vendor_code;
  this.orgname = organization.orgname;
  this.phoneno1 = organization.phoneno1;
  this.phoneno2 = organization.phoneno2;
  this.mobileno1 = organization.mobileno1;
  this.mobileno2 = organization.mobileno2;
  this.mobileno3 = organization.mobileno3;
  this.whatsappno = organization.whatsappno;
  this.email = organization.email;
  this.email2 = organization.email2;
  this.website = organization.website;
  this.gstin = organization.gstin;
  this.pan = organization.pan;
  this.state_code = organization.state_code;
  this.gst_reg_type = organization.gst_reg_type;
  this.place_of_supply = organization.place_of_supply;
  this.nature_of_bussiness = organization.nature_of_bussiness;
  this.year_of_establishment = organization.year_of_establishment;
  this.branch = organization.branch;
  this.esic_reg_no = organization.esic_reg_no;
  this.epf_reg_no = organization.epf_reg_no;
  this.ssi_reg_no = organization.ssi_reg_no;
  this.company_reg = organization.company_reg;
  this.cin_reg = organization.cin_reg;
  this.msme_reg = organization.msme_reg;
  this.labour_reg = organization.labour_reg;
  this.time_created = organization.time_created;
  this.time_modified = organization.time_modified;
  this.stylish_of_company_id = organization.stylish_of_company_id;
};

// Create
Organization.create = (newOrganization, result) => {
  sql.query(
    queryGenertor.CREATE_ORGANIZATION_QUERY,
    newOrganization,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      console.log("Organization Created: ", {
        id: res.insertId,
        ...newOrganization,
      });
      result(null, { id: res.insertId, ...newOrganization });
    }
  );
};

// get All Data
Organization.getAll = (result) => {
  sql.query(queryGenertor.SELECT_ORGANIZATION_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    // console.log("Organization: ", res);
    result(null, res);
  });
};
Organization.createaddress = (newOrganizationaddress, result) => {
  console.log('createaddress');
  newOrganizationaddress.forEach((element,index) => {
    debugger;
    console.log("newOrganizationaddress", newOrganizationaddress.length, index);
    const {
      address,
      address_name: add_name,
      city,
      country,
      district,
      latitude,
      localityTown: locality,
      longitude,
      pinCode: postal,
      state,
      time_created,
      time_modified,
      org_id,
    } = element;
    const addr = {
      address,
      add_name,
      city,
      country,
      district,
      latitude,
      locality,
      longitude,
      postal,
      state,
      time_created,
      time_modified,
      org_id,
    };
    sql.query(
      queryGenertor.CREATE_ORGANIZATIONADDRESS_QUERY,
      addr,
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          if (newOrganizationaddress.length === index +1) {
            result(err, null);
            return;
          }
          
          
        }
        if (newOrganizationaddress.length === index +1) {
          result(null, true);
          return;
        }
      }
    );
  });
};
module.exports = Organization;
