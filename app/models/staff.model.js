const sql = require("./db.js");
const queryGenertor = require("../utils/queryGenertor");

// constructor
const Staff = function (staff) {
  this.firstname = staff.firstname;
  this.lastname = staff.lastname;
  this.username = staff.username;
  this.password = staff.password;
  this.dob = staff.dob;
  this.sex = staff.sex;
  this.phoneno = staff.phoneno;
  this.mobileno = staff.mobileno;
  this.whatsappno = staff.whatsappno;
  this.email = staff.email;
  this.bloodgroup = staff.bloodgroup;
  this.doj = staff.doj;
  this.adhaar = staff.adhaar;
  this.pan = staff.pan;
  this.res_address = staff.res_address;
  this.res_locality = staff.res_locality;
  this.res_city = staff.res_city;
  this.res_district = staff.res_district;
  this.res_state = staff.res_state;
  this.res_country = staff.res_country;
  this.res_pin = staff.res_pin;
  this.per_address = staff.per_address;
  this.per_locality = staff.per_locality;
  this.per_city = staff.per_city;
  this.per_district = staff.per_district;
  this.per_country = staff.per_country;
  this.per_pin = staff.per_pin;
  this.department_id = staff.department_id;
  this.designation_id = staff.designation_id;
  this.staffrole_id = staff.staffrole_id;
  this.emptype_id = staff.emptype_id;
  this.esino = staff.esino;
  this.pfno = staff.pfno;
  this.token - staff.token;
};

// Create
Staff.create = (newStaff, result) => {
  sql.query(queryGenertor.CREATE_STAFF_QUERY, newStaff, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("Staff Created: ", { id: res.insertId, ...newStaff });
    result(null, { id: res.insertId, ...newStaff });
  });
};

// get All Data
Staff.getAll = (result) => {
  sql.query(queryGenertor.SELECT_STAFF_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    // console.log("staff: ", res);
    result(null, res);
  });
};

// find By token
Staff.findByToken = (token, result) => {
  sql.query(`${queryGenertor.SELECT_STAFF_BY_TOKEN_QUERY} "${token}"`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


// find By Id
Staff.findById = (id, result) => {
  sql.query(`${queryGenertor.SELECT_STAFF_BY_ID_QUERY} "${id}"`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

// find by username and password
Staff.findByUsernameAndPassword = (reqObj, result) => {
  const { username, password } = reqObj;
  sql.query(
    `SELECT * FROM staff WHERE username = "${username}" AND password = "${password}"`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      if (res.length) {
        console.log("found customer: ", res[0]);
        result(null, res[0]);
        return;
      }

      // not found Customer with the id
      result({ kind: "not_found" }, null);
    }
  );
};

// Update
Staff.updateById = (id, staff, result) => {
  const {
    firstname,
    lastname,
    username,
    password,
    sex,
    dob,
    emails,
    phoneno,
    mobileno,
    whatsappno,
    res_address,
    res_locality,
    res_city,
    res_district,
    res_state,
    res_country,
    res_pin,
    per_address,
    per_locality,
    per_city,
    per_district,
    per_state,
    per_country,
    per_pin,
    bloodgroup,
    doj,
    adhaar,
    pan,
    department_id,
    designation_id,
    staffrole_id,
    esino,
    pfno,
    emptype_id,
    time_modified,
  } = staff;
  sql.query(
    queryGenertor.updateStaff_Query,
    [
      firstname,
      lastname,
      username,
      password,
      sex,
      dob,
      emails,
      phoneno,
      mobileno,
      whatsappno,
      res_address,
      res_locality,
      res_city,
      res_district,
      res_state,
      res_country,
      res_pin,
      per_address,
      per_locality,
      per_city,
      per_district,
      per_state,
      per_country,
      per_pin,
      bloodgroup,
      doj,
      adhaar,
      pan,
      department_id,
      designation_id,
      staffrole_id,
      esino,
      pfno,
      emptype_id,
      time_modified,
      id,
    ],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated customer: ", { id: id, ...customer });
      result(null, { id: id, ...customer });
    }
  );
};


Staff.getStaffRoleById = async (id, result, callBack) => {
  sql.query(`${queryGenertor.GET_STAFF_BY_ID_QUERY} "${id}"`, (err, res) => {
  
    if (err) {
      console.log("error: ", err);
      return {
        status : false
      };
    } else {
      result.role = res[ 0].name
      callBack();
    }
  });
};

// get All Data
Staff.getStaffRole = (result) => {
  sql.query(queryGenertor.GET_STAFF_ROLE_QUERY, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    // console.log("staff: ", res);
    result(null, res);
  });
};

// delete API for STAFF by ID
Staff.remove = (id, result) => {
  sql.query(queryGenertor.DELETE_STAFF_QUERY, id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted customer with id: ", id);
    result(null, res);
  });
};

module.exports = Staff;
