const getDateTime = () => {
  return new Date();
};

const createJson = ({ data, message, statusCode, status }) => {
  return {
    data,
    message,
    status,
    status_code: statusCode,
  };
};

module.exports = {
  getDateTime,
  createJson
};
