const DEPARTMENT_QUERY = "SELECT * FROM Department";
const EMP_TYPE_QUERY = "SELECT * FROM EmployeeType";
const DESIGNATION_QUERY = "SELECT * FROM Designation";

// STAFF QUERY
const CREATE_STAFF_QUERY = "INSERT INTO staff SET ?";
const updateStaff_Query =
  "UPDATE `staff` SET `firstname`= ?,`lastname`= ?,`username`= ?,`password`= ?,`sex`= ?,`dob`= ?,`email`= ?,`phoneno`= ?,`mobileno`= ?,`whatsappno`= ?,`res_address`= ?,`res_locality`= ?,`res_city`= ?,`res_district`= ?,`res_state`= ?,`res_country`= ?,`res_pin`= ?,`per_address`= ?,`per_locality`= ?,`per_city`= ?,`per_district`= ?,`per_state`= ?,`per_country`= ?,`per_pin`= ?,`bloodgroup`= ?',`doj`= ?,`adhaar`= ?,`pan`= ?,`department_id`= ?,`designation_id`= ?,`staffrole_id`= ?,`esino`= ?,`pfno`= ?,`emptype_id`= ?,`time_modified`= ? WHERE `user_id` = ?";

const SELECT_STAFF_QUERY = "SELECT * FROM staff";
const SELECT_STAFF_BY_TOKEN_QUERY = "SELECT * FROM staff WHERE token =";
const SELECT_STAFF_BY_ID_QUERY = "SELECT * FROM staff WHERE user_id =";
const GET_STAFF_BY_ID_QUERY = "SELECT * FROM staff_role WHERE role_id =";
const GET_STAFF_ROLE_QUERY = "SELECT * FROM staff_role";
const DELETE_STAFF_QUERY = "DELETE FROM staff WHERE user_id = ?";

// Contact Querry
const ORGANIZATION_ROLES_QUERY = "SELECT * FROM organization_role";
const CREATE_CONTACT_QUERY = "INSERT INTO contact SET ?";
const SELECT_CONTACT_QUERY = "SELECT * FROM contact";
const SELECT_CONTACT_BY_ID_QUERY = "SELECT * FROM contact WHERE id =";
const GET_CONTACT_ROLE_BY_ID_QUERY =
  "SELECT * FROM organization_role WHERE id =";
const UPDATE_CONTACT_QUERY =
  "UPDATE `contact` SET `name`=?,`phoneno`=?,`mobileno`=?,`whatsappno`=?, `email`=?,`remarks`=?,`org_add_id`=?,`organization_id`=?,`role_id`=?  WHERE `id` = ?";
const DELETE_CONTACT_QUERY = "DELETE FROM contact WHERE id = ?";

// Enterprise Querry
const COMPANY_STYLE_QUERY = "SELECT * FROM stylish_of_company";
const CREATE_ENTERPRISE_QUERY = "INSERT INTO enterprise SET ?";
const SELECT_ENTERPRISE_QUERY = "SELECT * FROM enterprise";
const SELECT_ENTERPRISE_BY_ID_QUERY =
  "SELECT * FROM enterprise WHERE enterprise_id =";
const UPDATE_ENTERPRISE_QUERY =
  "UPDATE `enterprise` SET `enterpisename`=?,`phoneno1`=?,`phoneno2`=?,`mobileno1`=?, `mobileno2`=?,`mobileno3`=?,`whatsappno`=?,`email1`=?,`email2`=?,`website`=?,`gstin`=?,`pan`=?,`state_code`=?,`gst_reg_type`=?,`place_of_supply`=?,`nature_of_bussiness`=?,`year_of_establishment`=?,`branch`=?,`esic_reg_no`=?,`epf_reg_no`=?,`ssi_reg_no`=?,`company_reg`=?,`cin_reg`=?,`msme_reg`=?,`labour_reg`=?,`address`=?,`locality`=?,`city`=?,`district`=?,`postal`=?,`state`=?,`country`=?,`latitude`=?,`longtidue`=?,`bank_name`=?,`account_number`=?,`ifsc_code`=?,`bank_branch`=?,`micr_code_number`=?,`type_of_account`=?,`beneficial_name`=?,`company_style`=?  WHERE `enterprise_id`= ?";
const DELETE_ENTERPRISE_QUERY =
  "DELETE FROM enterprise WHERE enterprise_id = ?";

// Enquiry Query
const ENQUIRY_TYPES_QUERY = "SELECT * FROM enquiryapp_enquirytype";
const ENQUIRY_SOURCES_QUERY = "SELECT * FROM enquiryapp_enquirysource";

// for creating select query
// params : tableName (which table you want to fetch the data)
// isWhereClause (true / false value . if value is true you can send whereclause is param which attribute you want select)
const Select_Query_URL_Creater = ({
  tableName,
  isWhereClause = false,
  whereClause,
}) => {
  let query = `SELECT * FROM ${tableName}`;
  if (isWhereClause) {
    query = `SELECT * FROM ${tableName} WHERE ${whereClause} =`;
  }
  return query;
};

// for creating insert query
const insert_Query_URL_Creater = ({ tableName }) => {
  return `INSERT INTO ${tableName} SET ?`;
};

// for creating delete query
const delete_QUERY_URL_Creater = ({ tableName, whereClause }) => {
  return `DELETE FROM ${tableName} WHERE ${whereClause} = ?`;
};

const SELECT_ACTIVITY_TYPE_QUERY =
  "SELECT * FROM enquiryapp_activitytypes WHERE type LIKE ";
const SELECT_SEARCH_ENQUIRY_QUERY =
  "SELECT * FROM enquiry WHERE Organization LIKE ";

const SELECT_ENTQUIRY_BY_ID_QUERY = "SELECT * FROM enquiry WHERE id =";

const UPDATE_ENQUIRY_QUERY =
  "UPDATE `enquiry` SET `enquirytype`=?,`enquirystatus`=?,`activitytype`=?,`sitesurveyschedule_date`=?, `organization`=?,`contact`=?,`enquirysource`=?,`scheduledate`=?,`comment`=?,`remarks`=?,`staff`=?,`org_address_id`=?  WHERE `id` = ?";

module.exports = {
  updateStaff_Query,
  DEPARTMENT_QUERY,
  EMP_TYPE_QUERY,
  DESIGNATION_QUERY,
  CREATE_STAFF_QUERY,
  SELECT_STAFF_QUERY,
  SELECT_STAFF_BY_ID_QUERY,
  GET_STAFF_BY_ID_QUERY,
  GET_STAFF_ROLE_QUERY,
  DELETE_STAFF_QUERY,
  SELECT_STAFF_BY_TOKEN_QUERY,
  Select_Query_URL_Creater,
  insert_Query_URL_Creater,
  delete_QUERY_URL_Creater,
  ORGANIZATION_ROLES_QUERY,
  CREATE_CONTACT_QUERY,
  SELECT_CONTACT_QUERY,
  SELECT_CONTACT_BY_ID_QUERY,
  GET_CONTACT_ROLE_BY_ID_QUERY,
  UPDATE_CONTACT_QUERY,
  DELETE_CONTACT_QUERY,
  COMPANY_STYLE_QUERY,
  CREATE_ENTERPRISE_QUERY,
  SELECT_ENTERPRISE_QUERY,
  SELECT_ENTERPRISE_BY_ID_QUERY,
  UPDATE_ENTERPRISE_QUERY,
  DELETE_ENTERPRISE_QUERY,
  ENQUIRY_TYPES_QUERY,
  ENQUIRY_SOURCES_QUERY,
  SELECT_ACTIVITY_TYPE_QUERY,
  SELECT_SEARCH_ENQUIRY_QUERY,
  SELECT_ENTQUIRY_BY_ID_QUERY,
  UPDATE_ENQUIRY_QUERY,
};
