
const Enquiry = require('../models/enquiry.model')
const jwt = require("jsonwebtoken");
const common = require("../utils/common");


exports.getEnquiryStatus = (req,res) => {
  Enquiry.getEnquiryStatusModel((err, data) => {
      let resObject = {};
      let arrayNew =[]
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Enquiry fetched",
          status: true,
        });
        data.forEach(data => {
          arrayNew.push(data.status)
        });
      }
      const Obj = {
        data:arrayNew
      }
      res.send(Obj);
    });
};

exports.get_Enq_Types = (req, res) => {
    Enquiry.getEnqTypes((err, data) => {
      let resObject = {};
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Enquiry Types fetched",
          status: true,
        });
      }
      res.send(JSON.stringify(resObject));
    });
  };

  exports.get_Enq_Sources = (req, res) => {
    Enquiry.getEnqSources((err, data) => {
      let resObject = {};
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
      } else {
      
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Enquiry Sources fetched",
          status: true,
        });
      

      }
      res.send(JSON.stringify(resObject));
    });
  };

  
  exports.get_Activity_types = (req, res) => {
    Enquiry.getActivityTypes((err, data) => {
      let resObject = {};
      let arrayNew =[]
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Activity Types fetched",
          status: true,
        });
        data.forEach(data => {
          arrayNew.push(data.type)
        });
      }
      const Obj = {
        data:arrayNew
      }
      res.send(Obj);
    });
  };

  exports.createActivity =(req,res)=>{
if(!req.body){
  res.status(400).send({
    message: "Content can not be empty!",
  });
}
const {
  activityType:type="",
} = req.body;
const time_created = common.getDateTime();
const time_modified = time_created;
const ActivityType ={
  type
}
Enquiry.activityCreate (ActivityType,(err,data)=>{
  let resObject ={}
  if(err){
    resObject = common.createJson({
      data: err,
      statusCode: "200",
      message: "Activity Already Exist!",
      status: false,
    });
    res.send(JSON.stringify(resObject));
  }else {
    resObject = common.createJson({
      data,
      statusCode: "200",
      message: "Activity added successfully!",
      status: true,
    });
    res.send(JSON.stringify(resObject));
  }
})
};

exports.deleteActivity = (req, res) => {
  const { activityType } = req.body;
  if (activityType) {
    Enquiry.activityDelete(activityType, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "Activity Type not found",
              data: [],
            })
          );
        } else {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "Could not deleteActivity Type with id",
              data: [],
            })
          );
        }
      } else {
        res.send(
          common.createJson({
            status: true,
            status_code: "200",
            message: "Activity Type Deleted successfully!",
            data: [],
          })
        );
      }
    });
  } else {
    res.send(
      common.createJson({
        status: false,
        status_code: "400",
        message: "Please pass id",
        data: [],
      })
    );
  }
};


exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  const {
    enquirytype = "",
    enquirystatus = "",
    activitytype = "",
    sitesurveyschedule_date = "",
    organization = "",
    contact = "",
    enquirysource = "",
    scheduledate = "",
    comment = "",
    remarks="",
    staff="",
    org_address_id=""
  } = req.body;
  const time_created = common.getDateTime();
  const time_modified = time_created;

  const enquiry = {
    enquirytype,
    enquirystatus,
    activitytype,
    sitesurveyschedule_date,
    organization,
    contact,
    enquirysource,
    scheduledate,
    comment,
    remarks,
    staff,
    org_address_id
  };
  Enquiry.create(enquiry, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "enquiry add failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "enquiry added successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};

// Update
exports.update = (req, res) => {
  const {
    id,
    enquirytype = "",
    enquirystatus = "",
    activitytype = "",
    sitesurveyschedule_date = "",
    organization = "",
    contact = "",
    enquirysource = "",
    scheduledate = "",
    comment = "",
    remarks="",
    staff="",
    org_address_id=""
  } = req.body;
  const time_created = common.getDateTime();
  const time_modified = time_created;

  const enquiry = {
    id,
    enquirytype,
    enquirystatus,
    activitytype,
    sitesurveyschedule_date,
    organization,
    contact,
    enquirysource,
    scheduledate,
    comment,
    remarks,
    staff,
    org_address_id
  };
  Enquiry.updateById(id,enquiry, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "enquiry add failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "enquiry added successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};

exports.findAll = (req, res) => {
  const {
    Organization_name:organization =""
  } =req.body
  const Organization =
  {
    organization
  }
  Enquiry.getEnquiries(Organization,(err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Enquiry fetched successfully!",
        status: true,
      });
      resObject.enquiry = data
      res.send(JSON.stringify(resObject));
    } 
  });
};

exports.findByID = (req, res) => {
  const { enquiry_id: enquiryId } = req.body;
  let reqObject = {};
  if (enquiryId) {
    Enquiry.findById(enquiryId, (err, data) => {
      console.log("COMING BACK", err, data);
      if (err) {
        reqObject = common.createJson({
          data: err,
          message: "data fetched failed",
          statusCode: "400",
          status: false,
        });
        res.send(JSON.stringify(reqObject));
      } else {
        reqObject = common.createJson({
          data,
          message: "data fetched successfully",
          statusCode: "200",
          status: true,
        });
        // reqObject.data = data;
        // contact.getContactRoleById(data.role_id,reqObject.data, () => {
        //   res.send(JSON.stringify(reqObject));
        // });
        res.send(JSON.stringify(reqObject));
      }
    });
  } else {
    reqObject = common.createJson({
      data: [],
      message: "Please send Token in headers",
      statusCode: "400",
      status: true,
    });
    res.send(JSON.stringify(reqObject));
  }
};


exports.getAll = (req, res) => {
  Enquiry.getReqLists((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Enquiry fetched successfully!",
        status: true,
      });
      resObject.enquiry = data
      res.send(JSON.stringify(resObject));
    } 
  });
};
