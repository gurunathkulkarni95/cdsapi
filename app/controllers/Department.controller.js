const Department = require("../models/Department.model");
const designation = require("../models/Designation.model");
const staff = require("../models/staff.model");
const common = require("../utils/common");

// get All Department
exports.get_department = (req, res) => {
  Department.getDepartment((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: false,
      });
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Department fetched",
        status: true,
      });
    }
    res.send(JSON.stringify(resObject));
  });
};


exports.get_empType = (req, res) => {
    Department.getEmpType((err, data) => {
      let resObject = {};
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: false,
        });
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "emp_type fetched",
          status: true,
        });
      }
      res.send(JSON.stringify(resObject));
    });
  };

  exports.commonAPI = (req,res) => {
    let resObject = {};
    Department.getDepartment((err, data) => {
      if (err) {
        resObject = common.createJson({
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: false,
        });
      } else {
        resObject["department"] = data;
        Department.getEmpType((err, emp) => {
          if (err) {
            resObject = common.createJson({
              data,
              statusCode: "400",
              message: "Some error occurred while retrieving!",
              status: false,
            });
          } else {
            resObject["emp_type"] = emp
            designation.getDesignation((err, des) => {
              if (err) {
                resObject = common.createJson({
                  data,
                  statusCode: "400",
                  message: "Some error occurred while retrieving!",
                  status: true,
                });
              } else {
                resObject["designation"] = des
                staff.getStaffRole((err, role) => {
                  if (err) {
                    resObject = common.createJson({
                      data,
                      statusCode: "400",
                      message: "Some error occurred while retrieving!",
                      status: true,
                    });
                  } else {
                    resObject["role"] = role;
                    res.send(JSON.stringify(resObject));
                  }
                });
              }
              
            });
          }
        });
      }
      
    });
    

  }
