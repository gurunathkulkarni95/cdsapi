const contact = require("../models/contact.model");
const jwt = require("jsonwebtoken");
const common = require("../utils/common");

exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  const {
    name = "",
    phoneno = "",
    mobileno = "",
    whatsappno = "",
    email = "",
    remarks = "",
    org_add_id = "",
    organization_id = "",
    role_id = "",
  } = req.body;
  const time_created = common.getDateTime();
  const time_modified = time_created;

  const Contact = {
    name,
    phoneno,
    mobileno,
    whatsappno,
    email,
    remarks,
    org_add_id,
    organization_id,
    role_id,
  };

  contact.create(Contact, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "Contact add failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Contact added successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};

  // get All contacts
  exports.findAll = (req, res) => {
    contact.getAll((err, data) => {
      let resObject = {};
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
        res.send(JSON.stringify(resObject));
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Contacts fetched successfully!",
          status: true,
        });
        res.send(JSON.stringify(resObject));
      }
      
    });
  };

  // get contact by id
exports.findByID = (req, res) => {
  const { contact_id: contactId } = req.body;
  let reqObject = {};
  if (contactId) {
    contact.findById(contactId, (err, data) => {
      console.log("COMING BACK", err, data);
      if (err) {
        reqObject = common.createJson({
          data: err,
          message: "data fetched failed",
          statusCode: "400",
          status: false,
        });
        res.send(JSON.stringify(reqObject));
      } else {
        reqObject = common.createJson({
          data,
          message: "data fetched successfully",
          statusCode: "200",
          status: true,
        });
        reqObject.data = data;
        contact.getContactRoleById(data.role_id,reqObject.data, () => {
          res.send(JSON.stringify(reqObject));
        });
        // res.send(JSON.stringify(reqObject));
      }
    });
  } else {
    reqObject = common.createJson({
      data: [],
      message: "Please send Token in headers",
      statusCode: "400",
      status: true,
    });
    res.send(JSON.stringify(reqObject));
  }
};

// Update Contact

exports.update =(req,res)=>{
  const {
    id="",
    name = "",
    phoneno = "",
    mobileno = "",
    whatsappno = "",
    email = "",
    remarks = "",
    org_add_id = "",
    organization_id = "",
    role_id = "",
  } = req.body;

  const Contact = {
    id,
    name,
    phoneno,
    mobileno,
    whatsappno,
    email,
    remarks,
    org_add_id,
    organization_id,
    role_id,
   
  };
  contact.updateById(id, Contact, (err, data) => {
    let resObj = {};
    if (err) {
      resObj = common.createJson({
        data: err,
        message: "data updated failed",
        statusCode: "400",
        status: false,
      });
      res.send(JSON.stringify(resObj));
    } else {
      resObj = common.createJson({
        data,
        message: "success",
        statusCode: "200",
        status: true,
      });
      res.send(JSON.stringify(resObj));
    }
  });
}

// delete contact by id
exports.delete = (req, res) => {
  const { id } = req.body;
  if (id) {
    contact.remove(id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "Contact not found",
              data: [],
            })
          );
        } else {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "Could not delete Contact with id",
              data: [],
            })
          );
        }
      } else {
        res.send(
          common.createJson({
            status: true,
            status_code: "200",
            message: "Contact Deleted successfully!",
            data: [],
          })
        );
      }
    });
  } else {
    res.send(
      common.createJson({
        status: false,
        status_code: "400",
        message: "Please pass id",
        data: [],
      })
    );
  }
};