const staff = require("../models/staff.model");
const jwt = require("jsonwebtoken");
const common = require("../utils/common");

//login API
exports.Login = (req, res) => {
  const { username, password } = req.body;
  let resObj = {};
  if (username && password) {
    const reObj = {
      username,
      password,
    };
    staff.findByUsernameAndPassword(reObj, (err, data) => {
      if (err) {
        resObj = common.createJson({
          data: err,
          message: "data fetched failed",
          statusCode: "400",
          status: false,
        });
        res.send(JSON.stringify(resObj));
      } else {
        resObj = common.createJson({
          data,
          message: "success",
          statusCode: "200",
          status: true,
        });
        resObj.user_details = data;
        staff.getStaffRoleById(data.staffrole_id, resObj.user_details, () => {
          res.send(JSON.stringify(resObj));
        });
      }
    });
  } else {
    resObj = common.createJson({
      data: [],
      message: "Please send username and password",
      statusCode: "400",
      status: true,
    });
    res.send(JSON.stringify(resObj));
  }
};

// Add Staff to staff table
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  const {
    firstname = "",
    lastname = "",
    username = "",
    password = "",
    dob = "",
    sex = "",
    phoneno = "",
    mobileno = "",
    whatsappno = "",
    email = "",
    bloodgroup = "",
    doj = "",
    adhaar = "",
    pan = "",
    res_address = "",
    res_locality = "",
    res_city = "",
    res_district = "",
    res_state = "",
    res_country = "",
    res_pin = "",
    per_address = "",
    per_locality = "",
    per_city = "",
    per_district = "",
    per_state = "",
    per_country = "",
    per_pin = "",
    department_id = "",
    designation_id = "",
    staffrole_id = "",
    emptype_id = "",
    esino = "",
    pfno = "",
  } = req.body;

  const combainUsernameAndPassword = `${username}${password}`;

  const token = jwt.sign({ username: combainUsernameAndPassword }, "shhhhh");
  const time_created = common.getDateTime();
  const time_modified = time_created;
  // Create a Staff
  const Staff = {
    firstname,
    lastname,
    dob,
    sex,
    phoneno,
    mobileno,
    whatsappno,
    email,
    bloodgroup,
    doj,
    adhaar,
    pan,
    res_address,
    res_locality,
    res_city,
    res_district,
    res_state,
    res_country,
    res_pin,
    per_address,
    per_locality,
    per_city,
    per_district,
    per_state,
    per_country,
    per_pin,
    department_id,
    designation_id,
    staffrole_id,
    emptype_id,
    esino,
    pfno,
    time_created,
    time_modified,
    password,
    username,
    token,
  };

  staff.create(Staff, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "Staff added failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Staff added successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};

// get All Staff
exports.findAll = (req, res) => {
  staff.getAll((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Staff searched successfully!",
        status: true,
      });
      
      res.send(JSON.stringify(resObject));
    }
    
  });
};

// get Staff by token
exports.findOne = (req, res) => {
  const { authorization: token } = req.headers;
  let reqObject = {};
  if (token) {
    staff.findById(token, (err, data) => {
      console.log("COMING BACK", err, data);
      if (err) {
        reqObject = common.createJson({
          data: err,
          message: "data fetched failed",
          statusCode: "400",
          status: false,
        });
        res.send(JSON.stringify(reqObject));
      } else {
        reqObject = common.createJson({
          data,
          message: "data fetched successfully",
          statusCode: "200",
          status: true,
        });
        res.send(JSON.stringify(reqObject));
      }
    });
  } else {
    reqObject = common.createJson({
      data: [],
      message: "Please send Token in headers",
      statusCode: "400",
      status: true,
    });
    res.send(JSON.stringify(reqObject));
  }
};


// get Staff by user_id
exports.findByID = (req, res) => {
  const { staff_id: staffId } = req.body;
  let reqObject = {};
  if (staffId) {
    staff.findById(staffId, (err, data) => {
      console.log("COMING BACK", err, data);
      if (err) {
        reqObject = common.createJson({
          data: err,
          message: "data fetched failed",
          statusCode: "400",
          status: false,
        });
        res.send(JSON.stringify(reqObject));
      } else {
        reqObject = common.createJson({
          data,
          message: "data fetched successfully",
          statusCode: "200",
          status: true,
        });
        res.send(JSON.stringify(reqObject));
      }
    });
  } else {
    reqObject = common.createJson({
      data: [],
      message: "Please send Token in headers",
      statusCode: "400",
      status: true,
    });
    res.send(JSON.stringify(reqObject));
  }
};

// update Staff
exports.update = (req, res) => {
  const {
    user_id = "",
    firstname = "",
    lastname = "",
    username = "",
    password = "",
    dob = "",
    sex = "",
    phoneno = "",
    mobileno = "",
    whatsappno = "",
    email = "",
    bloodgroup = "",
    doj = "",
    adhaar = "",
    pan = "",
    res_address = "",
    res_locality = "",
    res_city = "",
    res_district = "",
    res_state = "",
    res_country = "",
    res_pin = "",
    per_address = "",
    per_locality = "",
    per_city = "",
    per_district = "",
    per_state = "",
    per_country = "",
    per_pin = "",
    department_id = "",
    designation_id = "",
    staffrole_id = "",
    emptype_id = "",
    esino = "",
    pfno = "",
  } = req.body;

  // Create a Staff
  const Staff = {
    user_id,
    firstname,
    lastname,
    username,
    dob,
    sex,
    phoneno,
    mobileno,
    whatsappno,
    email,
    bloodgroup,
    doj,
    adhaar,
    pan,
    res_address,
    res_locality,
    res_city,
    res_district,
    res_state,
    res_country,
    res_pin,
    per_address,
    per_locality,
    per_city,
    per_district,
    per_state,
    per_country,
    per_pin,
    department_id,
    designation_id,
    staffrole_id,
    emptype_id,
    esino,
    pfno,
    time_created,
    time_modified,
    password,
    token,
  };

  staff.updateById(user_id, Staff, (err, data) => {
    let resObj = {};
    if (err) {
      resObj = common.createJson({
        data: err,
        message: "data updated failed",
        statusCode: "400",
        status: false,
      });
      res.send(JSON.stringify(resObj));
    } else {
      resObj = common.createJson({
        data,
        message: "success",
        statusCode: "200",
        status: true,
      });
      res.send(JSON.stringify(resObj));
    }
  });
};

// get All Staffrole
exports.get_Staff_Role = (req, res) => {
  staff.getStaffRole((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "Staff role fetched",
        status: true,
      });
    }
    res.send(JSON.stringify(resObject));
  });
};

// delete saff by id
exports.delete = (req, res) => {
  const { id } = req.body;
  if (id) {
    staff.remove(id, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "staff not found",
              data: [],
            })
          );
        } else {
          res.send(
            common.createJson({
              status: false,
              status_code: "400",
              message: "Could not delete staff with id",
              data: [],
            })
          );
        }
      } else {
        res.send(
          common.createJson({
            status: true,
            status_code: "200",
            message: "Staff Deleted successfully!",
            data: [],
          })
        );
      }
    });
  } else {
    res.send(
      common.createJson({
        status: false,
        status_code: "400",
        message: "Please pass id",
        data: [],
      })
    );
  }
};
