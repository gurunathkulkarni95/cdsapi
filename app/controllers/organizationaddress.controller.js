const Organization = require("../models/organizationaddress.model");
const jwt = require("jsonwebtoken");
const common = require("../utils/common");
const OrganizationAddress = require("../models/organizationaddress.model");

// Add organization to organization table
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  const {
    add_name = "",
    address = "",
    locality = "",
    city = "",
    district = "",
    postal = "",
    state = "",
    country = "",
    org_id = "",
    latitude = "",
    longitude = ""
  } = req.body;

  //   const combainUsernameAndPassword = `${username}${password}`;
  //   const token = jwt.sign({ username: combainUsernameAndPassword }, "shhhhh");
   const time_created = common.getDateTime();
   const time_modified = time_created;
  // Create an Organization
  const organizationaddress = {
    add_name,
    address,
    locality,
    city,
    district,
    postal,
    state,
    country,
    org_id,
    latitude,
    longitude,
    time_created,
    time_modified,
  };

  OrganizationAddress.create(organizationaddress, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "Org added failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "organization added successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};

// get All organization
exports.findAll = (req, res) => {
  OrganizationAddress.getAll((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "organization searched successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};
