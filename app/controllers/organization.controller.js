const Organization = require("../models/organization.model");
const jwt = require("jsonwebtoken");
const common = require("../utils/common");

// Add organization to organization table
exports.create = (req, res) => {
  var addressflag = false;
  let bankflag = false;
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }
  const {
    org_type = "",
    vendor_code = "",
    name: orgname = "",
    phoneno1 = "",
    phoneno2 = "",
    mobileno1 = "",
    mobileno2 = "",
    mobileno3 = "",
    whatsAppNo: whatsappno = "",
    email1: email = "",
    email2 = "",
    website = "",
    gstnumber: gstin = "",
    pan = "",
    state_code = "",
    gst_reg_type = "",
    place_of_supply = "",
    natureofbusiness: nature_of_bussiness = "",
    yearofestablishment: year_of_establishment = "",
    branch = "",
    ESICregnumber: esic_reg_no = "",
    EPFregnumber: epf_reg_no = "",
    SSIregnumber: ssi_reg_no = "",
    Companyregnumber: company_reg = "",
    CINregnumber: cin_reg = "",
    MSMEregnumber: msme_reg = "",
    Labourregnumber: labour_reg = "",
    stylish_of_company: stylish_of_company_id = "",
    addresses = [],
    bankDetails = [],
  } = req.body;
  console.log("address", addresses, bankDetails);

  //   const combainUsernameAndPassword = `${username}${password}`;
  //   const token = jwt.sign({ username: combainUsernameAndPassword }, "shhhhh");
  const time_created = common.getDateTime();
  const time_modified = time_created;
  // Create an Organization
  const organization = {
    org_type,
    vendor_code,
    orgname,
    phoneno1,
    phoneno2,
    mobileno1,
    mobileno2,
    mobileno3,
    whatsappno,
    email,
    email2,
    website,
    gstin,
    pan,
    state_code,
    gst_reg_type,
    place_of_supply,
    nature_of_bussiness,
    year_of_establishment,
    branch,
    esic_reg_no,
    epf_reg_no,
    ssi_reg_no,
    company_reg,
    cin_reg,
    msme_reg,
    labour_reg,
    time_created,
    time_modified,
    stylish_of_company_id,
  };

  Organization.create(organization, (err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data: err,
        statusCode: "200",
        message: "Org added failed!",
        status: false,
      });
      res.send(JSON.stringify(resObject));
    } else {
      addresses.forEach((element) => {
        element.org_id = data.id;
      });
      console.log("data", addresses);

      Organization.createaddress(addresses, (err, addrdata) => {
        
        if (err) {
          console.log("err", err);
        } else {
          console.log("address  success");
        }

      });
      // resObject = common.createJson({
      //   data,
      //   statusCode: "200",
      //   message: "organization added successfully!",
      //   status: true,
      // });
      // res.send(JSON.stringify(resObject));
    }
  });
};

// get All organization
exports.findAll = (req, res) => {
  Organization.getAll((err, data) => {
    let resObject = {};
    if (err) {
      resObject = common.createJson({
        data,
        statusCode: "400",
        message: "Some error occurred while retrieving!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    } else {
      resObject = common.createJson({
        data,
        statusCode: "200",
        message: "organization searched successfully!",
        status: true,
      });
      res.send(JSON.stringify(resObject));
    }
  });
};
