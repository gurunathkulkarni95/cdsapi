const designation = require("../models/Designation.model");
const jwt = require("jsonwebtoken");
const common = require("../utils/common");


// get All Staffrole
exports.get_Designation = (req, res) => {
    designation.getDesignation((err, data) => {
      let resObject = {};
      if (err) {
        resObject = common.createJson({
          data,
          statusCode: "400",
          message: "Some error occurred while retrieving!",
          status: true,
        });
      } else {
        resObject = common.createJson({
          data,
          statusCode: "200",
          message: "Designation fetched",
          status: true,
        });
      }
      res.send(JSON.stringify(resObject));
    });
  };