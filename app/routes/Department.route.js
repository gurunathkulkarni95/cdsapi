module.exports = (app) => {
  const Department = require("../controllers/Department.controller");

  // Retrieve all Customers
  app.get("/department", Department.get_department);

  app.get("/emp_type", Department.get_empType);

  app.get("/common_api", Department.commonAPI)
};
