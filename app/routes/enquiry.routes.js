module.exports = (app) => {
  const enquiry = require("../controllers/enquiry.controller");

  // Retrieve all Styles of Company
  app.get("/enquiry_types", enquiry.get_Enq_Types);

  app.get("/enquiry_sources", enquiry.get_Enq_Sources);
  // Retrieve all Customers
  app.get("/enquirystatus", enquiry.getEnquiryStatus);

  app.get("/enquiry_activitytype", enquiry.get_Activity_types);
  app.post("/createActivity",enquiry.createActivity)
  app.post("/deleteActivity",enquiry.deleteActivity)

  app.post("/addEnquiry",enquiry.create)
  app.post("/enquiries",enquiry.findAll)
  app.post('/getenquiry',enquiry.findByID)
  app.get("/requestlist",enquiry.getAll)
  app.put('/updateenquiry',enquiry.update)


  // // Retrieve all Customers
  // app.post("/addenquiry", enquiry.addEnquiry);
};
