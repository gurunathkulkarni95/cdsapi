module.exports = (app) => {
  const Designation = require("../controllers/Designation.controller");

  // Retrieve all Customers
  app.get("/designation", Designation.get_Designation);
};
