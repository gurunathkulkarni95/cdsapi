module.exports =(app)=>{

    const contact = require("../controllers/contact.controller");
    // Create a new Contact
  app.post("/contact", contact.create);

   // Retrieve all Contacts
 app.get("/contact", contact.findAll);

   // Retrieve a single Contact with Id
   app.post("/getcontact", contact.findByID);

    // Update a Customer with customerId
  app.put("/updatecontact", contact.update);

  // Delete a Contact with Id
  app.post("/contact_delete", contact.delete);
}