module.exports = (app) => {
  const staff = require("../controllers/staff.controller.js");

  // LOGIN API
  app.post("/login", staff.Login);

  // Create a new Customer
  app.post("/staff", staff.create);

  // Retrieve all Customers
  app.get("/staff", staff.findAll);

  // Retrieve a single Customer with customerId
  app.post("/getstaff", staff.findByID);

  // Staff role API
  app.get("/staff_role", staff.get_Staff_Role);

  // Update a Customer with customerId
  app.put("/staff", staff.update);

  // Delete a Customer with customerId
  app.post("/staff_delete", staff.delete);
};
