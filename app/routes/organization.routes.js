module.exports = (app) => {
    const organization = require("../controllers/organization.controller.js");
  
  
    // Create a new Org
    app.post("/organization", organization.create);
  
    // Retrieve all Orgs
    app.get("/organization", organization.findAll);

  

  };
  