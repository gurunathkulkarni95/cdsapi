

module.exports = (app) => {
    const organizationaddress = require("../controllers/organizationaddress.controller.js");
  
  
    // Create a new Org
    app.post("/organizationaddress", organizationaddress.create);
  
    // Retrieve all Orgs
    app.get("/organizationaddress", organizationaddress.findAll);

  

  };
  